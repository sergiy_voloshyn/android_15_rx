package com.sourceit.myapplicationrx;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Observable.range(1, 1000).toList().subscribe(
                new Action1<List<Integer>>() {
                    @Override
                    public void call(List<Integer> arrayList) {
                    }
                });

        Observable.range(1, 1000)
                        .map(new Func1<Integer, String>() {
                    @Override
                    public String call(Integer integer) {
                        return String.valueOf(integer);
                    }
                })


        .filter(new Func1<String, Boolean>() {
            @Override
            public Boolean call(String s) {
                return s.contains("3");
            }
        })
                .count().subscribe(
                new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        Toast.makeText(MainActivity.this,String.valueOf(integer),Toast.LENGTH_LONG).show();
                    }
                }
        );



    }
}
